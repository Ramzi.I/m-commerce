package com.clientui.exceptions;

class Product4XXException extends RuntimeException {
    Product4XXException(String message) {
        super(message);
    }
}
